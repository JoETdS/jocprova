package dam2021.joc.recolzament;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

public class AssetManager {

    // Fulla d'imatges
    public static Texture textura;

    // Imatge de la nau
    public static TextureRegion nau, nauBaix, nauDalt;
    public static TextureRegion fonsDePantalla;

    // Asteroides
    public static TextureRegion[] asteroides;
    public static Animation animationAsteroides;

    // Explosió
    public static TextureRegion[] explosions;
    public static Animation animationExplosio;



    public static void load() {

        // Carreguem les textures
        textura = new Texture(Gdx.files.internal("imatges.png"));
        textura.setFilter(Texture.TextureFilter.Nearest,
                Texture.TextureFilter.Nearest);

        // Imatges de la nau
        nau = new TextureRegion(textura, 0, 0, 36, 15);
        nau.flip(false, true);

        nauDalt = new TextureRegion(textura, 36, 0, 36, 15);
        nauDalt.flip(false, true);

        nauBaix = new TextureRegion(textura, 72, 0, 36, 15);
        nauBaix.flip(false, true);

        // Imatges de l'asteroide
        asteroides = new TextureRegion[16];
        for (int i = 0; i < asteroides.length; i++) {
            asteroides[i] = new TextureRegion(textura,
                    i * 34, 15, 34, 34);
            asteroides[i].flip(false, true);
        }

        // Animació asteroide
        animationAsteroides = new Animation(0.05f, asteroides);
        animationAsteroides.setPlayMode(Animation.PlayMode.LOOP_REVERSED);

        // Imatges de l'explosió
        explosions = new TextureRegion[16];

        int index = 0;
        for (int i = 0; i < 2; i++) {
            for (int j = 0; j < 8; j++) {
                explosions[index++] = new TextureRegion(
                        textura, j * 64,
                        i * 64 + 49, 64, 64);
                explosions[index - 1].flip(false, true);
            }
        }

        animationExplosio = new Animation(0.04f, explosions);
        animationExplosio.setPlayMode(Animation.PlayMode.NORMAL);

        // Imatge del fons
        fonsDePantalla = new TextureRegion(textura,
                0, 177, 480, 135);
        }

    public static void dispose() {

        // Descartem les imatges
        textura.dispose();

    }
}
