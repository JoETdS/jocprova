package dam2021.joc.recolzament;

import com.badlogic.gdx.InputProcessor;

import dam2021.joc.objectes.Nau;
import dam2021.joc.pantalles.GameScreen;

public class InputHandler implements InputProcessor {

    // Objectes necessaris
    private Nau nau;
    private GameScreen pantalla;

    // Valors previs
    private int previaY = 0;

    public InputHandler(GameScreen pantalla){
        this.pantalla = pantalla;
        this.nau = this.pantalla.getNau();
    }


    @Override
    public boolean keyDown(int keycode) {
        return false;
    }

    @Override
    public boolean keyUp(int keycode) {
        return false;
    }

    @Override
    public boolean keyTyped(char character) {
        return false;
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {

        this.previaY = screenY;
        return true;
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {

        this.nau.noMoure();
        return true;
    }

    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer) {

        // Anem cap a dalt
        if (this.previaY > screenY){
            this.nau.anarDalt();
        }
        // Anemm cap a baix
        else{
            this.nau.anarBaix();
        }

        return true;
    }

    @Override
    public boolean mouseMoved(int screenX, int screenY) {
        return false;
    }

    @Override
    public boolean scrolled(float amountX, float amountY) {
        return false;
    }
}
