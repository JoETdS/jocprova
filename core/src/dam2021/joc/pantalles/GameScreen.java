package dam2021.joc.pantalles;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputAdapter;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.viewport.StretchViewport;

import java.util.ArrayList;

import dam2021.joc.objectes.Asteroide;
import dam2021.joc.objectes.Nau;
import dam2021.joc.objectes.ScrollHandler;
import dam2021.joc.recolzament.InputHandler;
import dam2021.joc.utilitats.Configuracio;

public class GameScreen implements Screen {

    private Nau nau;
    private ScrollHandler scrollHandler;
    private Stage stage;

    // Representació de les figures
    private ShapeRenderer renderitzadorDeFigures;
    private Batch conjunt;


    public GameScreen(){

        // Creem el ShapeRenderer
        this.renderitzadorDeFigures = new ShapeRenderer();

        // Creem la càmera de les dimensions del joc
        OrthographicCamera camera = new OrthographicCamera(
                Configuracio.AMPLADA_JOC, Configuracio.ALCADA_JOC);

        // Posem el paràmetre a true perquè la càmera faci servir l'Y a baix
        camera.setToOrtho(true);

        // Creem un viewport que ens permetrà veure què hi ha per pantalla
        StretchViewport viewport = new StretchViewport(
                Configuracio.AMPLADA_JOC, Configuracio.ALCADA_JOC, camera);
        // Escenari
        // Creem l'escenari
        // Assigna el viewport a l'escenari
        this.stage = new Stage(viewport);

        // Agafem el conjunt
        this.conjunt = stage.getBatch();

        // Actors
        // Creem la nau i els elements que es mouen per pantalla
        this.nau = new Nau(Configuracio.AMPLADA_NAU, Configuracio.ALCADA_NAU,
                Configuracio.X_INICIAL_NAU, Configuracio.Y_INICIAL_NAU);
        this.scrollHandler = new ScrollHandler();

        // Afegim els actors a l'escenari
        this.stage.addActor(this.scrollHandler);
        this.stage.addActor(this.nau);

        // Assignem el gestor d'entrada per pantalla
        Gdx.input.setInputProcessor(new InputHandler(this));
        
    }

    public Nau getNau() {
        return nau;
    }

    public ScrollHandler getScrollHandler() {
        return scrollHandler;
    }

    public Stage getStage() {
        return stage;
    }

    @Override
    public void show() {

    }

    @Override
    public void render(float delta) {

        // Dibuixem l'escenari i els seus actors
        this.stage.draw();
        this.stage.act(delta);

        if(scrollHandler.colisio(this.nau)){
            Gdx.app.log("EPPPPPP", "Explosio");
        }

        //dibuixaElsElements();

    }

    private void dibuixaElsElements() {
        // Pintem el fons negre
        //Gdx.gl.glClearColor(0, 0, 0, 1);
        //Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        // Recollim les propietats del conjunt de l'escenari
        renderitzadorDeFigures.setProjectionMatrix(
                conjunt.getProjectionMatrix());
        // Inicialitzem el rendiritzdor de figures
        renderitzadorDeFigures.begin(ShapeRenderer.ShapeType.Filled);

        // Dibuixem la nau
            // Decidim el color
        renderitzadorDeFigures.setColor(new Color(1, 0, 0, 1));
            // Pintem la nau
        renderitzadorDeFigures.rect(nau.getX(), nau.getY(),
                nau.getAmplada(), nau.getAlcada());

        // Dibuixem els asteroides
            // Decidim el color
        renderitzadorDeFigures.setColor(1, 1, 1, 1);
            // Agafem els asteroides
        ArrayList<Asteroide> asteroides = this.scrollHandler.getAsteroides();
        Asteroide asteroide;
            // Pintem els asteroides
        for (int i = 0; i < asteroides.size(); i++){
            asteroide = asteroides.get(i);
            renderitzadorDeFigures.circle(
                    asteroide.getX() + asteroide.getAmplada()/2,
                    asteroide.getY() + asteroide.getAmplada()/2,
                    asteroide.getAmplada()/2);
        }

        // Tanquem el rendereitzador
        renderitzadorDeFigures.end();

    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {

    }
}
