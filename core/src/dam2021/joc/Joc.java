package dam2021.joc;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

import dam2021.joc.pantalles.GameScreen;
import dam2021.joc.recolzament.AssetManager;

public class Joc extends Game {

	/*
	Primer mètode que s'executa a l'iniciar l'aplicació
	 */
	@Override
	public void create() {

		//A l'iniciar carreguem els recursos
		AssetManager.load();

		//Definim la pantalla principal del joc
		setScreen(new GameScreen());

	}
	/*
	Últim mètode que s'executa abans de tancar l'aplicació
 	*/
	@Override
	public void dispose() {
		super.dispose();
		AssetManager.dispose();
	}

	/*
	Retorna al darrer estat de l'aplicació si així ho volem
	*/
	@Override
	public void resume() {
		super.resume();
	}

	/*
	La lògica del joc serà implementada en aquest mètode
	Cridarem en bucle el mètode per repetir constament què volem passi
	 */
	@Override
	public void render() {
		super.render();
	}

	/*
	Quan l'aplicació perd el primer pla, s'executa
	 */
	public void pause(){

	}

}
