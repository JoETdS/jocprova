package dam2021.joc.utilitats;

public class Configuracio {

    // Mida del joc, automàticament s'escalarà
    public static final int AMPLADA_JOC = 240;
    public static final int ALCADA_JOC = 135;

    // Propietats de la nau
    public static final int AMPLADA_NAU = 35;
    public static final int ALCADA_NAU = 15;
    public static final float VELOCITAT_NAU = 50;
    public static final float X_INICIAL_NAU = 20;
    public static final float Y_INICIAL_NAU = ALCADA_JOC/2 - ALCADA_NAU/2;

    // Propietats del fons
    public static final float VELOCITAT_FONS = -100;

    // Propietats de l'asteroide
    public static final float VELOCITAT_ASTEROIDE = -150;
    public static final float MIDA_MAXIMA_ASTERIODE = 1.5f;
    public static final float MIDA_MINIMA_ASTERIODE = 0.5f;
    public static final float MIDA_ASTERIODE = 35;
    public static final float DISTANCIA_ENTRE_ASTERIODES = 75;

}
