package dam2021.joc.objectes;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;

public class Scrollable extends Actor {

    private Vector2 posicio;
    private float velocitat;
    private float amplada, alcada;
    private boolean foraDePantalla;

    public Scrollable(float x, float y, float velocitat, float amplada, float alcada){
        this.posicio = new Vector2(x, y);
        this.velocitat = velocitat;
        this.amplada = amplada;
        this.alcada = alcada;
        this.foraDePantalla = false;
    }


    public float getAmplada() {
        return amplada;
    }
    public void setAmplada(float amplada) {
        this.amplada = amplada;
    }

    public float getAlcada() {
        return alcada;
    }

    public void setAlcada(float alcada) {
        this.alcada = alcada;
    }

    public boolean isForaDePantalla() {
        return foraDePantalla;
    }
    public float getX(){
        return this.posicio.x;
    }

    public void setPosicio(Vector2 posicio) {
        this.posicio = posicio;
    }

    public float getCuaX(){
        return this.posicio.x + amplada;
    }

    public float getY(){
        return this.posicio.y;
    }

    public void act(float delta){
        // Desplacem l'objecte per l'eix X
        this.posicio.x += this.velocitat * delta;

        // Si es troba fora la pantalla canviem la variable a veritat
        if(getCuaX() < 0){
            this.foraDePantalla = true;
        }

    }

    public void reset(float novaX){
        this.posicio.x = novaX;
        foraDePantalla = false;
    }


}
