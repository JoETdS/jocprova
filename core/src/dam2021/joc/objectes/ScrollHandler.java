package dam2021.joc.objectes;

import com.badlogic.gdx.scenes.scene2d.Group;

import org.omg.CORBA.CODESET_INCOMPATIBLE;

import java.util.ArrayList;
import java.util.Random;

import dam2021.joc.utilitats.Configuracio;
import sun.security.krb5.Config;

public class ScrollHandler extends Group {

    // Fons de pantalla
    Background background, background_black;

    // Asteroides
    int numAsteroides;
    ArrayList<Asteroide> asteroides;

    Random r;

    public ScrollHandler() {
        //x, y, velocitat, amplada, alcada
        // Creem el fons
        this.background = new Background(0, 0, Configuracio.VELOCITAT_FONS,
                Configuracio.AMPLADA_JOC * 2, Configuracio.ALCADA_JOC);
        this.background_black = new Background(this.background.getCuaX(), 0, Configuracio.VELOCITAT_FONS,
                Configuracio.AMPLADA_JOC * 2, Configuracio.ALCADA_JOC);

        // Afegim els fons al grup
        addActor(background);
        addActor(background_black);

        // Creem els asteriodes

        this.r = new Random();

        // Nombre d'asteriodes
        this.numAsteroides = 3;

        // Creem la llista d'asterioides
        this.asteroides = new ArrayList<Asteroide>();

        // Definim mida asteriode
        float mida = novaMidaAsteroide();

        // Afegim el primer asteriode a l'ArrayList i al grup
        Asteroide asteroide = new Asteroide(Configuracio.AMPLADA_JOC,
                posicioYAsteroide(mida),
                Configuracio.VELOCITAT_ASTEROIDE, mida, mida);

        // Afegim l'asteroide a l'ArrayList
        this.asteroides.add(asteroide);

        // Afegim l'asteroide al grup d'actors
        addActor(asteroide);

        // Afegim la resta d'asteroides
        for (int i = 1; i < numAsteroides; i++) {
            // Definim mida asteriode
            mida = novaMidaAsteroide();

            // Afegim el primer asteriode a l'ArrayList i al grup
            Asteroide a = new Asteroide(
                    asteroides.get(asteroides.size() - 1).getCuaX() + Configuracio.DISTANCIA_ENTRE_ASTERIODES,
                    posicioYAsteroide(mida),
                    Configuracio.VELOCITAT_ASTEROIDE, mida, mida);

            // Afegim l'asteroide a l'ArrayList
            this.asteroides.add(a);

            // Afegim l'asteroide al grup d'actors
            addActor(a);
        }

    }

    public ArrayList<Asteroide> getAsteroides() {
        return asteroides;
    }

    @Override
    public void act(float delta) {
        super.act(delta);
        // Comprovem si algun element ha sortit de la pantalla
        if (background.isForaDePantalla()) {
            background.reset(background_black.getCuaX());
        } else if (background_black.isForaDePantalla()) {
            background_black.reset(background.getCuaX());
        }

        for (int i = 0; i < asteroides.size(); i++) {
            Asteroide a = asteroides.get(i);
            if (a.isForaDePantalla()) {
                if (i == 0) {
                    a.reset(asteroides.get(asteroides.size() - 1).getCuaX()
                            + Configuracio.DISTANCIA_ENTRE_ASTERIODES);
                } else {
                    a.reset(asteroides.get(i - 1).getCuaX()
                            + Configuracio.DISTANCIA_ENTRE_ASTERIODES);
                }
            }


        }


    }

    private float novaMidaAsteroide() {
        return (this.r.nextFloat() + Configuracio.MIDA_MINIMA_ASTERIODE)
                * Configuracio.MIDA_ASTERIODE;
    }

    private int posicioYAsteroide(float mida) {
        return (int) (this.r.nextFloat() * (Configuracio.ALCADA_JOC - mida));
    }

    public boolean colisio(Nau nau) {
        for (Asteroide a : this.asteroides) {
            if (a.colisio(nau)) {
                return true;
            }
        }
        return false;
    }
}
