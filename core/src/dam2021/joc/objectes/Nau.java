package dam2021.joc.objectes;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;

import dam2021.joc.recolzament.AssetManager;
import dam2021.joc.utilitats.Configuracio;

public class Nau extends Actor {

    // Atributs de la nau
    private int amplada, alcada;
    private Vector2 posicio;
    private int direccio;

    // Direcció de la nau
    public static final int DALT = 1;
    public static final int BAIX = 2;
    public static final int QUIETA = 0;

    // variable per a gestionar la colisio

    private Rectangle rectangleColisio;

    public Nau(int amplada, int alcada, float x, float y){

        // Inicialitzem els paràmetres de la Nau
        this.alcada = alcada;
        this.amplada = amplada;
        this.posicio = new Vector2(x, y);

        // Marquem la direcció inicial
        this.direccio = QUIETA;

        //
        this.rectangleColisio = new Rectangle();

    }

    @Override
    public void act(float delta) {

        // Moviment de la nau
        switch(this.direccio){
            case DALT:
                if(this.posicio.y - Configuracio.VELOCITAT_NAU * delta >= 0){
                    this.posicio.y -= Configuracio.VELOCITAT_NAU * delta;
                }
                break;
            case BAIX:
                if(this.posicio.y + this.alcada + Configuracio.VELOCITAT_NAU * delta <= Configuracio.ALCADA_JOC){
                    this.posicio.y += Configuracio.VELOCITAT_NAU * delta;
                }
                break;
            case QUIETA:
                break;
        }

        this.rectangleColisio.set(getX(),
                getY() + 3,
                getAmplada(),
                10);
    }

    // Getters dels atributs de la nau

    public int getAmplada() {
        return amplada;
    }

    public int getAlcada() {
        return alcada;
    }

    public float getX() {
        return posicio.x;
    }

    public float getY() {
        return posicio.y;
    }

    public Rectangle getRectangleColisio(){
        return rectangleColisio;
    }

    // Canvis de direcció

    // Anar cap a dalt
    public void anarDalt(){
        this.direccio = DALT;
    }

    public void anarBaix(){
        this.direccio = BAIX;
    }

    public void noMoure(){
        this.direccio = QUIETA;
    }


    @Override
    public void draw(Batch batch, float parentAlpha){
        super.draw(batch, parentAlpha);
        batch.draw(getTexturaNau(), this.getX(), this.getY(),
                this.getAmplada(), this.getAlcada());
    }

    private TextureRegion getTexturaNau() {
        switch (this.direccio){
            case QUIETA:
                return AssetManager.nau;
            case DALT:
                return AssetManager.nauDalt;
            case BAIX:
                return AssetManager.nauBaix;
            default:
                return AssetManager.nau;

        }
    }

}
