package dam2021.joc.objectes;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Circle;
import com.badlogic.gdx.math.Intersector;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;

import java.security.cert.Certificate;
import java.util.Random;

import dam2021.joc.recolzament.AssetManager;
import dam2021.joc.utilitats.Configuracio;

public class Asteroide extends Scrollable {

    private Random r = new Random();
    private float temps;

    private Circle cercleColisio;

    public Asteroide(float x, float y, float velocitat, float amplada, float alcada) {
        super(x, y, velocitat, amplada, alcada);
        this.temps = r.nextFloat();
        this.cercleColisio = new Circle();
    }

    public Circle getCercleColisio() {
        return cercleColisio;
    }

    @Override
    public void act(float delta){
        super.act(delta);
        this.temps += delta;

        this.cercleColisio.set(getX()+getAmplada()/2,
                getY()+getAmplada()/2,
                getAmplada()/2);

    }

    @Override
    public void reset(float novaX) {
        super.reset(novaX);
        // Mida nou asteroide
        float mida = novaMidaAsteroide();
        // Dimensions nou asteroide
        setAmplada(mida);
        setAlcada(mida);

        // modifiquem la seva posició (valor Y)
        Vector2 novaPos = novaPosicio(mida);
        setPosicio(novaPos);
    }

    private Vector2 novaPosicio(float mida) {
        return new Vector2(getX(), (int) (this.r.nextFloat() * (Configuracio.ALCADA_JOC - mida)));
    }

    private float novaMidaAsteroide(){
        return (this.r.nextFloat() + Configuracio.MIDA_MINIMA_ASTERIODE)
                * Configuracio.MIDA_ASTERIODE;
    }

    public float getTemps(){
        return this.temps;
    }

    @Override
    public void draw(Batch batch, float parentAlpha){
        super.draw(batch, parentAlpha);
        batch.draw((TextureRegion) AssetManager.animationAsteroides.
                        getKeyFrame(this.temps),
                this.getX(), this.getY(), this.getAmplada(),
                this.getAlcada());

    }

    public boolean colisio(Nau nau) {
        if(getX() <= nau.getX() + nau.getAmplada()){
            return (Intersector.overlaps(this.cercleColisio, nau.getRectangleColisio()));
        }
        return false;

    }
}
