package dam2021.joc.objectes;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.scenes.scene2d.Actor;

import dam2021.joc.recolzament.AssetManager;

public class Background extends Scrollable {
    public Background(float x, float y, float velocitat, float amplada, float alcada) {
        super(x, y, velocitat, amplada, alcada);
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        super.draw(batch, parentAlpha);
        batch.disableBlending();
        batch.draw(AssetManager.fonsDePantalla, getX(), getY(),
                getAmplada(), getAlcada());
        batch.enableBlending();

    }
}
